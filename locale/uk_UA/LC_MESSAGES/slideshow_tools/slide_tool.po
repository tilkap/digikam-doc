# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-02-06 11:21+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../slideshow_tools/slide_tool.rst:1
msgid "Using digiKam Basic Slide Tool"
msgstr "Використання базового інструмента показу слайдів digiKam"

#: ../../slideshow_tools/slide_tool.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, slide"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, слайд"

#: ../../slideshow_tools/slide_tool.rst:14
msgid "Basic Slide Show"
msgstr "Базовий показ слайдів"

#: ../../slideshow_tools/slide_tool.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../slideshow_tools/slide_tool.rst:18
msgid ""
"This tool render a series of items as a basic slide-show. To run this tool "
"you can use the menu entry :menuselection:`View --> Slideshow` sub-menus or "
"simply press the **Show FullScreen** button on top of Icon-View item."
msgstr ""
"За допомогою цього інструмента можна обробити послідовність записів як показ "
"слайдів. Щоб запустити інструмент, скористайтеся підменю пункту меню :"
"menuselection:`Перегляд --> Перегляд слайдів` або просто натисніть кнопку "
"**Показати на увесь екран** у верхній частині пункту таблиці панелі "
"перегляду піктограм."

#: ../../slideshow_tools/slide_tool.rst:24
msgid "The Icon-View Item Show FullScreen Overlay Button"
msgstr ""
"Для запису панелі перегляду піктограм показано кнопку повноекранного "
"перегляду"

#: ../../slideshow_tools/slide_tool.rst:28
msgid ""
"The **Show FullScreen** button will be visible only if you turn on the right "
"option from :menuselection:`Settings --> Configure digiKam...` and **Icons** "
"tab from **Views** panel."
msgstr ""
"Кнопку **Показати на весь екран** буде показано, лише якщо ви позначите "
"відповідний пункт у вікні :menuselection:`Параметри --> Налаштувати "
"digiKam...`, вкладка **Піктограми** панелі **Перегляди**."

#: ../../slideshow_tools/slide_tool.rst:30
msgid ""
"The basic slideshow tool will render items on full screen without visual "
"effects and without zooming support. It is powerful to review quickly album "
"items. This tool can play album contents in recursive mode with children "
"albums if any."
msgstr ""
"На базовому рівні інструмент створення показів слайдів розгорне зображення "
"на увесь екран без візуальних ефектів і підтримки масштабування. Це потужний "
"засіб швидкого перегляду записів альбому. Інструмент може відтворювати вміст "
"альбому у рекурсивному режимі із дочірніми альбомами, якщо такі було "
"створено."

#: ../../slideshow_tools/slide_tool.rst:36
msgid "The Basic Slide-Show View Displaying Item and Properties"
msgstr "У базовому перегляді показу слайдів показано запис і його властивості"

#: ../../slideshow_tools/slide_tool.rst:38
msgid ""
"A lots of items properties can be displayed as overlay while displaying "
"contents. These ones are shown on the bottom left side as an **OSD (On "
"Screen Display)**."
msgstr ""
"Під час показу на накладці до зображення може бути показано цілий діапазон "
"його властивостей. Цю накладку буде показано у нижній лівій частині "
"зображення як екранну панель."

#: ../../slideshow_tools/slide_tool.rst:44
msgid ""
"The Basic Slide-Show Provides an OSD to Show Details and Control the Contents"
msgstr ""
"У базовому перегляді слайдів показано екранну панель-накладу із параметрами "
"запис та засобами керування даними"

#: ../../slideshow_tools/slide_tool.rst:46
msgid ""
"The basic slide show configuration should be easy to understand. The upper "
"slider adjusts the time between image transitions; usually a time of 4-5 "
"seconds is good. The other check boxes enable/disable the metadata to be "
"shown on the bottom of the slide show images during display."
msgstr ""
"Налаштування показу слайдів має бути інтуїтивно зрозумілим. За допомогою "
"верхнього повзунка можна скоригувати час між перемиканням зображень. "
"Зазвичай, можна зупинитися на значенні 4-5 секунд. За допомогою інших "
"пунктів ви можете увімкнути або вимкнути показ певних метаданих у нижній "
"частині області показу слайдів."

#: ../../slideshow_tools/slide_tool.rst:50
msgid ""
"The **Shuffle Images** mode is only available in automatic playback, i.e. "
"when you start the slide show via the menu or toolbar button. It does not "
"work in **Preview** mode when you start on the **Play** button icon in the "
"thumbnail or image preview."
msgstr ""
"Режимом **Випадковий порядок** можна скористатися лише при автоматичному "
"відтворенні, тобто тоді, коли ви запускаєте показ слайдів за допомогою меню "
"або кнопки панелі інструментів. Він не працює у режимі **Попередній "
"перегляд**, коли ви розпочинаєте перегляд за допомогою кнопки «відтворити» "
"на мініатюрі або зображенні попереднього перегляду."

#: ../../slideshow_tools/slide_tool.rst:56
msgid "The Basic Slide-Show Configuration Dialog"
msgstr "Діалогове вікно налаштовування базового показу слайдів"

#: ../../slideshow_tools/slide_tool.rst:58
msgid ""
"The Usage from Keyboard and mouse to quickly navigate between items is "
"listen below:"
msgstr ""
"Дані щодо використання клавіатури і миші для швидкої навігації між записами "
"наведено нижче:"

#: ../../slideshow_tools/slide_tool.rst:60
msgid "Item Access"
msgstr "Доступ до записів"

#: ../../slideshow_tools/slide_tool.rst:63
msgid ""
":kbd:`Up` key :kbd:`PgUp` key :kbd:`Left` key Mouse wheel up Left mouse "
"button"
msgstr ""
"Клавіша :kbd:`↑`, клавіша PgUp, клавіша :kbd:`←`, гортання коліщатка миші "
"вгору, ліва кнопка миші"

#: ../../slideshow_tools/slide_tool.rst:67
msgid "Previous Item:"
msgstr "Попередній запис:"

#: ../../slideshow_tools/slide_tool.rst:70
msgid ""
":kbd:`Down` key :kbd:`PgDown` key :kbd:`Right` key Mouse wheel down Right "
"mouse button"
msgstr ""
"Клавіша :kbd:`↓`, клавіша :kbd:`PgDown`, клавіша :kbd:`→`, гортання "
"коліщатка миші вниз, права кнопка миші"

#: ../../slideshow_tools/slide_tool.rst:74
msgid "Next Item:"
msgstr "Наступний запис:"

#: ../../slideshow_tools/slide_tool.rst:77
msgid "Pause/Start:"
msgstr "Пауза/Пуск:"

#: ../../slideshow_tools/slide_tool.rst:77
msgid ":kbd:`Space` key"
msgstr "Клавіша :kbd:`Пробіл`"

#: ../../slideshow_tools/slide_tool.rst:80
msgid "Slideshow Settings:"
msgstr "Параметри показу слайдів:"

#: ../../slideshow_tools/slide_tool.rst:80
msgid ":kbd:`F2` key"
msgstr "Клавіша :kbd:`F2`"

#: ../../slideshow_tools/slide_tool.rst:83
msgid "Hide/Show Properties:"
msgstr "Сховати/Показати властивості:"

#: ../../slideshow_tools/slide_tool.rst:83
msgid ":kbd:`F4` key"
msgstr "Клавіша :kbd:`F4`"

#: ../../slideshow_tools/slide_tool.rst:86
msgid "Quit:"
msgstr "Вийти:"

#: ../../slideshow_tools/slide_tool.rst:86
msgid ":kbd:`Esc` key"
msgstr "Клавіша :kbd:`Esc`"

#: ../../slideshow_tools/slide_tool.rst:88
msgid "Item Properties"
msgstr "Властивості запису"

#: ../../slideshow_tools/slide_tool.rst:91
msgid "Change Tags:"
msgstr "Зміна міток:"

#: ../../slideshow_tools/slide_tool.rst:91
msgid "Use Tags keyboard shortcuts"
msgstr "Використовуйте клавіатурні скорочення міток"

#: ../../slideshow_tools/slide_tool.rst:94
msgid "Change Rating:"
msgstr "Зміна оцінки:"

#: ../../slideshow_tools/slide_tool.rst:94
msgid "Use Rating keyboard shortcuts"
msgstr "Використовуйте клавіатурні скорочення оцінок"

#: ../../slideshow_tools/slide_tool.rst:97
msgid "Change Color Label:"
msgstr "Зміна кольорової мітки:"

#: ../../slideshow_tools/slide_tool.rst:97
msgid "Use Color label keyboard shortcuts"
msgstr "Використовуйте клавіатурні скорочення кольорових міток"

#: ../../slideshow_tools/slide_tool.rst:100
msgid "Change Pick Label:"
msgstr "Змінити впорядкувальну позначку:"

#: ../../slideshow_tools/slide_tool.rst:100
msgid "Use Pick label keyboard shortcuts"
msgstr "Використовуйте клавіатурні скорочення впорядкувальних позначок"

#: ../../slideshow_tools/slide_tool.rst:102
msgid "Others"
msgstr "Інші"

#: ../../slideshow_tools/slide_tool.rst:104
msgid "Show help dialog:"
msgstr "Показ вікна довідки:"

#: ../../slideshow_tools/slide_tool.rst:105
msgid ":kbd:`F1` key"
msgstr "Клавіша :kbd:`F1`"

# Translation of docs_digikam_org_main_window.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-09 15:29+0100\n"
"Last-Translator: Antoni Bella <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Kate 22.08.3\n"

#: ../../main_window.rst:1
msgid "Using the digiKam Main Window to Show Collection Contents"
msgstr ""
"Usar la finestra principal del digiKam per a mostrar el contingut de la "
"col·lecció"

#: ../../main_window.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"help, learn, main window, search, tags, labels, maps, date, timeline, "
"similarity, face, management"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, ajuda, aprenentatge, finestra principal, cerca, etiquetes, "
"rètols, mapes, data, línia de temps, semblança, cara, gestió"

#: ../../main_window.rst:15
msgid "Main Window"
msgstr "Finestra principal"

#: ../../main_window.rst:19
msgid "This section explain how to use the digiKam main window."
msgstr ""
"Aquesta secció explica com utilitzar la finestra principal en el digiKam."

#: ../../main_window.rst:21
msgid "Contents:"
msgstr "Contingut:"

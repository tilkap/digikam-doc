# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-11 10:00+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: digiKam menuselection Del kbd\n"

#: ../../setup_application/shortcuts_settings.rst:1
msgid "digiKam Shortcuts Settings"
msgstr "Configuração dos Atalhos do digiKam"

#: ../../setup_application/shortcuts_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, keyboard, shortcuts, setup, configure"
msgstr ""
"digiKam, documentação, manual do utilizador, gestão de fotografias, código "
"aberto, livre, aprender, fácil, teclado, atalhos, configuração"

#: ../../setup_application/shortcuts_settings.rst:14
msgid "Shortcuts Settings"
msgstr "Configuração dos Atalhos"

#: ../../setup_application/shortcuts_settings.rst:16
msgid "Contents"
msgstr "Conteúdo"

#: ../../setup_application/shortcuts_settings.rst:19
#, fuzzy
#| msgid "Shortcut"
msgid "Shortcuts Editor"
msgstr "Atalho"

#: ../../setup_application/shortcuts_settings.rst:21
msgid ""
"digiKam provide keyboard shortcuts that allow you to perform many tasks "
"without touching your mouse. If you use your keyboard frequently, using "
"these can save you lots of time."
msgstr ""
"O digiKam fornece atalhos de teclado que lhe permitem efectuar várias "
"tarefas sem tocar no seu rato. Se usar o seu teclado com frequência, se usar "
"este poderá poupar bastante tempo."

#: ../../setup_application/shortcuts_settings.rst:23
#, fuzzy
#| msgid ""
#| "digiKam has dedicated keyboard shortcut configuration dialog accessed via "
#| "the :menuselection:`Settings --> Configure Shortcuts` main menu item."
msgid ""
"digiKam has a dedicated keyboard shortcuts configuration dialog accessed via "
"the :menuselection:`Settings --> Configure Shortcuts` main menu item."
msgstr ""
"O digiKam tem uma janela de configuração de atalhos de teclado dedicada que "
"pode ser acedida através do item de menu :menuselection:`Configuração --> "
"Configurar os Atalhos`."

#: ../../setup_application/shortcuts_settings.rst:29
msgid "The digiKam Dialog To Configure The Keyboard Shortcuts"
msgstr "A Janela do digiKam para Configurar os Atalhos de Teclado"

#: ../../setup_application/shortcuts_settings.rst:31
msgid ""
"In this dialog, you will see a list of all the shortcuts available in the "
"application. You can use the **Search** box at the top to search for the "
"shortcut you want."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:33
msgid ""
"To change a shortcut, first click on the name of a shortcut you want to "
"change. You will see a radio group where you can choose whether to set the "
"shortcut to its default value, or select a new shortcut for the selected "
"action. To set a new shortcut, choose **Custom** and click on the button "
"next to it. Then just type the shortcut you would like to use, and your "
"changes will be saved."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:35
msgid ""
"To reset a shortcut, there is a button at the bottom of the dialog, called "
"**Defaults**. Clicking on this button will reset all your custom shortcuts "
"to their default values. You can also reset an individual shortcut to its "
"default value by selecting it, and choosing the **Default** radio button."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:37
msgid ""
"To remove a shortcut, select it from the list, then click the black arrow "
"with a cross icon to the right of the button that allows you to select a "
"shortcut."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:39
msgid ""
"To print-out a list of shortcuts for easy reference by clicking the "
"**Print** button at the bottom of the dialog."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:41
msgid ""
"You can also work with **Schemes**. These ones are keyboard shortcuts "
"configuration profiles, so you can create several profiles with different "
"shortcuts and switch between these profiles easily. To see a menu allowing "
"you to edit schemes, click on the **Manage Schemes** button at the bottom of "
"the dialog. The following options will appear:"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:43
msgid "**Current Scheme**: Allows you to switch between your schemes."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:45
msgid ""
"**New**: Creates a new scheme. This opens a window that lets you select a "
"name for your new scheme."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:47
msgid "**Delete**: Deletes the current scheme."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:49
msgid "**More Actions**: Opens the following menu:"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:51
msgid ""
"**Save Shortcuts to scheme**: Save the current shortcuts to the current "
"scheme."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:52
msgid "**Export Scheme**: Exports the current scheme to a file."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:53
msgid "**Import Scheme**: Imports a scheme from a file."
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:56
msgid "Default Shortcuts"
msgstr "Atalhos por Omissão"

#: ../../setup_application/shortcuts_settings.rst:58
#, fuzzy
#| msgid "digiKam define these keyboard shortcuts by default:"
msgid "digiKam define these keyboard shortcuts by default."
msgstr "O digiKam define estes atalhos de teclado por omissão:"

#: ../../setup_application/shortcuts_settings.rst:61
#, fuzzy
#| msgid "Rating assignment:"
msgid "Rating Assignment"
msgstr "Atribuição de classificação:"

#: ../../setup_application/shortcuts_settings.rst:64
msgid "Rating"
msgstr "Classificação"

#: ../../setup_application/shortcuts_settings.rst:64
#: ../../setup_application/shortcuts_settings.rst:78
#: ../../setup_application/shortcuts_settings.rst:90
#: ../../setup_application/shortcuts_settings.rst:108
#: ../../setup_application/shortcuts_settings.rst:121
#: ../../setup_application/shortcuts_settings.rst:133
#: ../../setup_application/shortcuts_settings.rst:157
#: ../../setup_application/shortcuts_settings.rst:173
#: ../../setup_application/shortcuts_settings.rst:191
#: ../../setup_application/shortcuts_settings.rst:208
#: ../../setup_application/shortcuts_settings.rst:224
#: ../../setup_application/shortcuts_settings.rst:244
#: ../../setup_application/shortcuts_settings.rst:264
#: ../../setup_application/shortcuts_settings.rst:276
msgid "Shortcut"
msgstr "Atalho"

#: ../../setup_application/shortcuts_settings.rst:66
msgid "No rating"
msgstr "Sem classificação"

#: ../../setup_application/shortcuts_settings.rst:66
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+0`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:67
msgid "1 star"
msgstr "1 estrela"

#: ../../setup_application/shortcuts_settings.rst:67
#, fuzzy
#| msgid "Ctrl+1"
msgid ":kbd:`Ctrl+1`"
msgstr "Ctrl+1"

#: ../../setup_application/shortcuts_settings.rst:68
msgid "2 stars"
msgstr "2 estrelas"

#: ../../setup_application/shortcuts_settings.rst:68
#, fuzzy
#| msgid "Ctrl+2"
msgid ":kbd:`Ctrl+2`"
msgstr "Ctrl+2"

#: ../../setup_application/shortcuts_settings.rst:69
msgid "3 stars"
msgstr "3 estrelas"

#: ../../setup_application/shortcuts_settings.rst:69
#, fuzzy
#| msgid "Ctrl+3"
msgid ":kbd:`Ctrl+3`"
msgstr "Ctrl+3"

#: ../../setup_application/shortcuts_settings.rst:70
msgid "4 stars"
msgstr "4 estrelas"

#: ../../setup_application/shortcuts_settings.rst:70
#, fuzzy
#| msgid "Ctrl+4"
msgid ":kbd:`Ctrl+4`"
msgstr "Ctrl+4"

#: ../../setup_application/shortcuts_settings.rst:71
msgid "5 stars"
msgstr "5 estrelas"

#: ../../setup_application/shortcuts_settings.rst:71
#, fuzzy
#| msgid "Ctrl+5"
msgid ":kbd:`Ctrl+5`"
msgstr "Ctrl+5"

#: ../../setup_application/shortcuts_settings.rst:75
#, fuzzy
#| msgid "Pick label assignment:"
msgid "Pick Label Assignment"
msgstr "Atribuição de legendas de cores:"

#: ../../setup_application/shortcuts_settings.rst:78
msgid "Pick Label"
msgstr "Extracção da legenda"

#: ../../setup_application/shortcuts_settings.rst:80
#: ../../setup_application/shortcuts_settings.rst:92
msgid "None"
msgstr "Nenhuma"

#: ../../setup_application/shortcuts_settings.rst:80
msgid ":kbd:`Alt+0`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:81
msgid "Rejected"
msgstr "Rejeitada"

#: ../../setup_application/shortcuts_settings.rst:81
msgid ":kbd:`Alt+1`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:82
msgid "Pending"
msgstr "Pendente"

#: ../../setup_application/shortcuts_settings.rst:82
msgid ":kbd:`Alt+2`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:83
msgid "Accepted"
msgstr "Aceite"

#: ../../setup_application/shortcuts_settings.rst:83
msgid ":kbd:`Alt+3`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:87
#, fuzzy
#| msgid "Color label assignment:"
msgid "Color Label Assignment"
msgstr "Atribuição da legenda de cores:"

#: ../../setup_application/shortcuts_settings.rst:90
msgid "Color Label"
msgstr "Legenda da Cor"

#: ../../setup_application/shortcuts_settings.rst:92
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+0`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:93
msgid "Red"
msgstr "Vermelho"

#: ../../setup_application/shortcuts_settings.rst:93
#, fuzzy
#| msgid "Ctrl+Alt+1"
msgid ":kbd:`Ctrl+Alt+1`"
msgstr "Ctrl+Alt+1"

#: ../../setup_application/shortcuts_settings.rst:94
msgid "Orange"
msgstr "Laranja"

#: ../../setup_application/shortcuts_settings.rst:94
#, fuzzy
#| msgid "Ctrl+Alt+2"
msgid ":kbd:`Ctrl+Alt+2`"
msgstr "Ctrl+Alt+2"

#: ../../setup_application/shortcuts_settings.rst:95
msgid "Yellow"
msgstr "Amarelo"

#: ../../setup_application/shortcuts_settings.rst:95
#, fuzzy
#| msgid "Ctrl+Alt+3"
msgid ":kbd:`Ctrl+Alt+3`"
msgstr "Ctrl+Alt+3"

#: ../../setup_application/shortcuts_settings.rst:96
msgid "Green"
msgstr "Verde"

#: ../../setup_application/shortcuts_settings.rst:96
#, fuzzy
#| msgid "Ctrl+Alt+4"
msgid ":kbd:`Ctrl+Alt+4`"
msgstr "Ctrl+Alt+4"

#: ../../setup_application/shortcuts_settings.rst:97
msgid "Blue"
msgstr "Azul"

#: ../../setup_application/shortcuts_settings.rst:97
#, fuzzy
#| msgid "Ctrl+Alt+5"
msgid ":kbd:`Ctrl+Alt+5`"
msgstr "Ctrl+Alt+5"

#: ../../setup_application/shortcuts_settings.rst:98
msgid "Magenta"
msgstr "Magenta"

#: ../../setup_application/shortcuts_settings.rst:98
#, fuzzy
#| msgid "Ctrl+Alt+6"
msgid ":kbd:`Ctrl+Alt+6`"
msgstr "Ctrl+Alt+6"

#: ../../setup_application/shortcuts_settings.rst:99
msgid "Gray"
msgstr "Cinzento"

#: ../../setup_application/shortcuts_settings.rst:99
#, fuzzy
#| msgid "Ctrl+Alt+7"
msgid ":kbd:`Ctrl+Alt+7`"
msgstr "Ctrl+Alt+7"

#: ../../setup_application/shortcuts_settings.rst:100
msgid "Black"
msgstr "Preto"

#: ../../setup_application/shortcuts_settings.rst:100
#, fuzzy
#| msgid "Ctrl+Alt+8"
msgid ":kbd:`Ctrl+Alt+8`"
msgstr "Ctrl+Alt+8"

#: ../../setup_application/shortcuts_settings.rst:101
msgid "White"
msgstr "Branco"

#: ../../setup_application/shortcuts_settings.rst:101
#, fuzzy
#| msgid "Ctrl+Alt+9"
msgid ":kbd:`Ctrl+Alt+9`"
msgstr "Ctrl+Alt+9"

#: ../../setup_application/shortcuts_settings.rst:105
msgid "Zooming"
msgstr "Ampliação"

#: ../../setup_application/shortcuts_settings.rst:108
#: ../../setup_application/shortcuts_settings.rst:121
#: ../../setup_application/shortcuts_settings.rst:133
#: ../../setup_application/shortcuts_settings.rst:157
#: ../../setup_application/shortcuts_settings.rst:173
#: ../../setup_application/shortcuts_settings.rst:191
#: ../../setup_application/shortcuts_settings.rst:208
#: ../../setup_application/shortcuts_settings.rst:224
#: ../../setup_application/shortcuts_settings.rst:244
#: ../../setup_application/shortcuts_settings.rst:264
#: ../../setup_application/shortcuts_settings.rst:276
msgid "Action"
msgstr "Acção"

#: ../../setup_application/shortcuts_settings.rst:110
msgid "Zoom in"
msgstr "Ampliar"

#: ../../setup_application/shortcuts_settings.rst:110
#, fuzzy
#| msgid "Ctrl++"
msgid ":kbd:`Ctrl++`"
msgstr "Ctrl++"

#: ../../setup_application/shortcuts_settings.rst:111
msgid "Zoom out"
msgstr "Reduzir"

#: ../../setup_application/shortcuts_settings.rst:111
#, fuzzy
#| msgid "Ctrl+-"
msgid ":kbd:`Ctrl+-`"
msgstr "Ctrl+-"

#: ../../setup_application/shortcuts_settings.rst:112
msgid "Zoom 100%"
msgstr "Ampliação a 100%"

#: ../../setup_application/shortcuts_settings.rst:112
#, fuzzy
#| msgid "Ctrl+."
msgid ":kbd:`Ctrl+.`"
msgstr "Ctrl+."

#: ../../setup_application/shortcuts_settings.rst:113
msgid "Fit to window"
msgstr "Ajustar à janela"

#: ../../setup_application/shortcuts_settings.rst:113
#, fuzzy
#| msgid "Ctrl+Alt+E"
msgid ":kbd:`Ctrl+Alt+E`"
msgstr "Ctrl+Alt+E"

#: ../../setup_application/shortcuts_settings.rst:114
msgid "Fit to selection"
msgstr "Ajustar à selecção"

#: ../../setup_application/shortcuts_settings.rst:114
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+S`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:118
msgid "Slide-Show"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:123
msgid "Play All"
msgstr "Reproduzir Tudo"

#: ../../setup_application/shortcuts_settings.rst:123
msgid ":kbd:`F9`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:124
msgid "Play Selection"
msgstr "Reproduzir a Selecção"

#: ../../setup_application/shortcuts_settings.rst:124
#, fuzzy
#| msgid "Alt+F4"
msgid ":kbd:`Alt+F9`"
msgstr "Alt+F4"

#: ../../setup_application/shortcuts_settings.rst:125
msgid "Play with Sub-Albums"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:125
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+F9`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:126
msgid "Presentation"
msgstr "Apresentação"

#: ../../setup_application/shortcuts_settings.rst:126
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Alt+F9`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:130
msgid "Views Control"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:135
msgid "Albums View"
msgstr "Área dos Álbuns"

#: ../../setup_application/shortcuts_settings.rst:135
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F1`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:136
msgid "Tags View"
msgstr "Área de Marcas"

#: ../../setup_application/shortcuts_settings.rst:136
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F2`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:137
msgid "Labels View"
msgstr "Área de Legendas"

#: ../../setup_application/shortcuts_settings.rst:137
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F3`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:138
msgid "Dates view"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:138
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F4`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:139
msgid "Timeline View"
msgstr "Área da Linha Temporal"

#: ../../setup_application/shortcuts_settings.rst:139
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F5`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:140
msgid "Search View"
msgstr "Área de Pesquisa"

#: ../../setup_application/shortcuts_settings.rst:140
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F6`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:141
msgid "Similarity View"
msgstr "Área de Semelhanças"

#: ../../setup_application/shortcuts_settings.rst:141
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F7`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:142
msgid "Map View"
msgstr "Área do Mapa"

#: ../../setup_application/shortcuts_settings.rst:142
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F8`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:143
msgid "People View"
msgstr "Área de Pessoas"

#: ../../setup_application/shortcuts_settings.rst:143
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Ctrl+F9`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:144
msgid "Full Screen Mode"
msgstr "Modo de Ecrã Completo"

#: ../../setup_application/shortcuts_settings.rst:144
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Ctrl+Shift+F`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:145
msgid "Preview"
msgstr "Antevisão"

#: ../../setup_application/shortcuts_settings.rst:145
msgid ":kbd:`F3`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:146
msgid "Exit Preview Mode"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:146
#, fuzzy
#| msgid ":kbd:`F2`"
msgid ":kbd:`Esc`"
msgstr ":kbd:`F2`"

#: ../../setup_application/shortcuts_settings.rst:147
msgid "Toggle Left Side-bar"
msgstr "Comutar a Barra Lateral Esquerda"

#: ../../setup_application/shortcuts_settings.rst:147
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+Left`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:148
msgid "Toggle Right Side-bar"
msgstr "Comutar a Barra Lateral Direita"

#: ../../setup_application/shortcuts_settings.rst:148
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+Right`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:149
msgid "Refresh"
msgstr "Actualizar"

#: ../../setup_application/shortcuts_settings.rst:149
#, fuzzy
#| msgid ":kbd:`F2`"
msgid ":kbd:`F5`"
msgstr ":kbd:`F2`"

#: ../../setup_application/shortcuts_settings.rst:150
msgid "Turn On/Off Color Management View"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:150
msgid ":kbd:`F12`"
msgstr ":kbd:`F12`"

#: ../../setup_application/shortcuts_settings.rst:154
msgid "Main Tools"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:159
msgid "Open in Editor"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:159
msgid ":kbd:`F4`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:160
msgid "Open in Default Application"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:160
#, fuzzy
#| msgid "Ctrl+4"
msgid ":kbd:`Ctrl+F4`"
msgstr "Ctrl+4"

#: ../../setup_application/shortcuts_settings.rst:161
msgid "Light Table"
msgstr "Mesa de Luz"

#: ../../setup_application/shortcuts_settings.rst:161
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+L`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:162
msgid "Place on Light Table"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:162
msgid ":kbd:`Ctrl+L`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:163
msgid "Add to Light Table"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:163
msgid ":kbd:`Ctrl+Shift+L`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:164
msgid "Batch Queue Manager"
msgstr "Gestor da Fila de Lote"

#: ../../setup_application/shortcuts_settings.rst:164
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+B`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:165
msgid "Add to Current Queue"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:165
msgid ":kbd:`Ctrl+B`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:166
msgid "Add to new Queue"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:166
msgid ":kbd:`Ctrl+Shift+B`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:170
msgid "Items Navigation"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:175
msgid "Back"
msgstr "Recuar"

#: ../../setup_application/shortcuts_settings.rst:175
#, fuzzy
#| msgid "Alt+F4"
msgid ":kbd:`Alt+Left`"
msgstr "Alt+F4"

#: ../../setup_application/shortcuts_settings.rst:176
msgid "Forward"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:176
#, fuzzy
#| msgid "Alt+F4"
msgid ":kbd:`Alt+Right`"
msgstr "Alt+F4"

#: ../../setup_application/shortcuts_settings.rst:177
#, fuzzy
#| msgid "Import Images"
msgid "First Image"
msgstr "Importar as Imagens"

#: ../../setup_application/shortcuts_settings.rst:177
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+Home`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:178
#, fuzzy
#| msgid "Import Images"
msgid "Last Image"
msgstr "Importar as Imagens"

#: ../../setup_application/shortcuts_settings.rst:178
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+End`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:179
msgid "Next Left Side-bar Tab"
msgstr "Barra Lateral Esquerda Seguinte"

#: ../../setup_application/shortcuts_settings.rst:179
#, fuzzy
#| msgid "Ctrl+Alt+E"
msgid ":kbd:`Ctrl+Alt+End`"
msgstr "Ctrl+Alt+E"

#: ../../setup_application/shortcuts_settings.rst:180
msgid "Next Right Side-bar Tab"
msgstr "Barra Lateral Direita Seguinte"

#: ../../setup_application/shortcuts_settings.rst:180
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+PgDown`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:181
msgid "Next Image"
msgstr "Imagem Seguinte"

#: ../../setup_application/shortcuts_settings.rst:181
msgid ":kbd:`Space`"
msgstr ":kbd:`Espaço`"

#: ../../setup_application/shortcuts_settings.rst:182
msgid "Previous Image"
msgstr "Imagem Anterior"

#: ../../setup_application/shortcuts_settings.rst:182
msgid ":kbd:`Backspace`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:183
msgid "Previous Left Side-bar Tab"
msgstr "Barra Lateral Esquerda Anterior"

#: ../../setup_application/shortcuts_settings.rst:183
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+Home`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:184
msgid "Previous Right Side-bar Tab"
msgstr "Barra Lateral Direita Anterior"

#: ../../setup_application/shortcuts_settings.rst:184
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+PgUp`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:188
msgid "Item Manipulation"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:193
msgid "Rename Item"
msgstr "Mudar o Nome do Item"

#: ../../setup_application/shortcuts_settings.rst:193
msgid ":kbd:`F2`"
msgstr ":kbd:`F2`"

#: ../../setup_application/shortcuts_settings.rst:194
#, fuzzy
#| msgid "Rename"
msgid "Rename Album"
msgstr "Mudar o Nome"

#: ../../setup_application/shortcuts_settings.rst:194
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+F2`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:195
msgid "New Album"
msgstr "Novo Álbum"

#: ../../setup_application/shortcuts_settings.rst:195
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+N`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:196
msgid "Move Item to Trash"
msgstr "Enviar o Item para o Lixo"

#: ../../setup_application/shortcuts_settings.rst:196
msgid ":kbd:`Del`"
msgstr ":kbd:`Del`"

#: ../../setup_application/shortcuts_settings.rst:197
#, fuzzy
#| msgid "Delete items permanently"
msgid "Delete Item permanently"
msgstr "Apagar os itens de forma permanente"

#: ../../setup_application/shortcuts_settings.rst:197
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+Del`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:198
msgid "Rotate Item Left"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:198
#, fuzzy
#| msgid "Ctrl++"
msgid ":kbd:`Ctrl+Shift+Left`"
msgstr "Ctrl++"

#: ../../setup_application/shortcuts_settings.rst:199
msgid "Rotate Item Right"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:199
#, fuzzy
#| msgid "Ctrl++"
msgid ":kbd:`Ctrl+Shift+Right`"
msgstr "Ctrl++"

#: ../../setup_application/shortcuts_settings.rst:200
msgid "Flip Item Horizontally"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:200
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+*`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:201
msgid "Flip Item Vertically"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:201
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+/`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:205
msgid "Application Operations"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:210
msgid "Close Window"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:210
#, fuzzy
#| msgid "Alt+F4"
msgid ":kbd:`Alt+F4`"
msgstr "Alt+F4"

#: ../../setup_application/shortcuts_settings.rst:211
#, fuzzy
#| msgid "Fit to selection"
msgid "Quit Application"
msgstr "Ajustar à selecção"

#: ../../setup_application/shortcuts_settings.rst:211
#, fuzzy
#| msgid "Ctrl+Q"
msgid ":kbd:`Ctrl+Q`"
msgstr "Ctrl+Q"

#: ../../setup_application/shortcuts_settings.rst:212
msgid "Configure Application"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:212
#, fuzzy
#| msgid "Ctrl++"
msgid ":kbd:`Ctrl+Shift+,`"
msgstr "Ctrl++"

#: ../../setup_application/shortcuts_settings.rst:213
#, fuzzy
#| msgid "The digiKam Dialog To Configure The Keyboard Shortcuts"
msgid "Configure Keyboard Shortcuts"
msgstr "A Janela do digiKam para Configurar os Atalhos de Teclado"

#: ../../setup_application/shortcuts_settings.rst:213
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:214
msgid "Show Menubar"
msgstr "Mostrar o Menu"

#: ../../setup_application/shortcuts_settings.rst:214
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+M`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:215
msgid "Show Thumbbar"
msgstr "Mostrar a Barra de Miniaturas"

#: ../../setup_application/shortcuts_settings.rst:215
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+T`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:216
msgid "Find Action in Menu"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:216
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+I`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:217
msgid "What's This?"
msgstr "O Que é Isto?"

#: ../../setup_application/shortcuts_settings.rst:217
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Shift+F1`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:221
msgid "Post Processing"
msgstr "Pós-Processamento"

#: ../../setup_application/shortcuts_settings.rst:226
msgid "Edit Album Properties"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:226
#, fuzzy
#| msgid "Alt+F4"
msgid ":kbd:`Alt+Return`"
msgstr "Alt+F4"

#: ../../setup_application/shortcuts_settings.rst:227
msgid "Edit Comments"
msgstr "Editar os Comentários"

#: ../../setup_application/shortcuts_settings.rst:227
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+C`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:228
msgid "Edit Titles"
msgstr "Editar os Títulos"

#: ../../setup_application/shortcuts_settings.rst:228
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+T`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:229
#, fuzzy
#| msgid "Fit to selection"
msgid "Edit Geolocation"
msgstr "Ajustar à selecção"

#: ../../setup_application/shortcuts_settings.rst:229
msgid ":kbd:`Ctrl+Shift+G`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:230
msgid "Edit Metadata"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:230
msgid ":kbd:`Ctrl+Shift+M`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:231
msgid "Assign Tag"
msgstr "Atribuir uma Marca"

#: ../../setup_application/shortcuts_settings.rst:231
msgid ":kbd:`T`"
msgstr ":kbd:`T`"

#: ../../setup_application/shortcuts_settings.rst:232
msgid "Show Assigned Tags"
msgstr "Mostrar as Marcas Atribuídas"

#: ../../setup_application/shortcuts_settings.rst:232
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+A`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:233
msgid "Adjust Date and Time"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:233
msgid ":kbd:`Ctrl+Shift+D`"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:234
msgid "Create Html gallery"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:234
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+H`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:235
msgid "Search Text"
msgstr "Procurar o Texto"

#: ../../setup_application/shortcuts_settings.rst:235
#, fuzzy
#| msgid "Ctrl+4"
msgid ":kbd:`Ctrl+F`"
msgstr "Ctrl+4"

#: ../../setup_application/shortcuts_settings.rst:236
msgid "Advanced Search"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:236
#, fuzzy
#| msgid "Ctrl+Alt+0"
msgid ":kbd:`Ctrl+Alt+F`"
msgstr "Ctrl+Alt+0"

#: ../../setup_application/shortcuts_settings.rst:237
msgid "Find Duplicates"
msgstr "Procurar Duplicados"

#: ../../setup_application/shortcuts_settings.rst:237
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+D`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:241
msgid "Export Tools"
msgstr "Ferramentas de Exportação"

#: ../../setup_application/shortcuts_settings.rst:246
msgid "Export to Box"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:246
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+B`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:247
msgid "Export to Dropbox"
msgstr "Exportar para o Dropbox"

#: ../../setup_application/shortcuts_settings.rst:247
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+D`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:248
msgid "Export to Flickr"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:248
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+R`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:249
msgid "Export to Google Drive"
msgstr "Exportar para o Google Drive"

#: ../../setup_application/shortcuts_settings.rst:249
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+G`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:250
msgid "Export to Google Photos"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:250
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+P`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:251
msgid "Export to Imageshack"
msgstr "Exportar para o Imageshack"

#: ../../setup_application/shortcuts_settings.rst:251
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+M`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:252
msgid "Export to iNaturalist"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:252
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+N`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:253
msgid "Export to local storage"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:253
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+L`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:254
msgid "Export to Onedrive"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:254
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+O`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:255
msgid "Export to Pinterest"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:255
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+I`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:256
msgid "Export to remote storage"
msgstr "Exportar para um armazenamento remoto"

#: ../../setup_application/shortcuts_settings.rst:256
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+K`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:257
msgid "Export to SmugMug"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:257
#, fuzzy
#| msgid "Ctrl+Alt+S"
msgid ":kbd:`Ctrl+Alt+Shift+S`"
msgstr "Ctrl+Alt+S"

#: ../../setup_application/shortcuts_settings.rst:261
msgid "Import Tools"
msgstr "Ferramentas de Importação"

#: ../../setup_application/shortcuts_settings.rst:266
#, fuzzy
#| msgid "Import Images"
msgid "Add Images"
msgstr "Importar as Imagens"

#: ../../setup_application/shortcuts_settings.rst:266
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+I`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:267
msgid "Import from Google Photos"
msgstr ""

#: ../../setup_application/shortcuts_settings.rst:267
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+P`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:268
msgid "Import from remote storage"
msgstr "Importar de um armazenamento remoto"

#: ../../setup_application/shortcuts_settings.rst:268
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+K`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:269
msgid "Import from SmugMug"
msgstr "Importação do SmugMug"

#: ../../setup_application/shortcuts_settings.rst:269
#, fuzzy
#| msgid "Shift+Del"
msgid ":kbd:`Alt+Shift+S`"
msgstr "Shift+Del"

#: ../../setup_application/shortcuts_settings.rst:273
#, fuzzy
#| msgid "Default Shortcuts"
msgid "Selection Operations"
msgstr "Atalhos por Omissão"

#: ../../setup_application/shortcuts_settings.rst:278
msgid "Copy"
msgstr "Copiar"

#: ../../setup_application/shortcuts_settings.rst:278
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+C`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:279
msgid "Cut"
msgstr "Cortar"

#: ../../setup_application/shortcuts_settings.rst:279
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+X`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:280
msgid "Paste"
msgstr "Colar"

#: ../../setup_application/shortcuts_settings.rst:280
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+V`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:281
msgid "Invert Selection"
msgstr "Inverter a Selecção"

#: ../../setup_application/shortcuts_settings.rst:281
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+I`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:282
msgid "Select All"
msgstr "Seleccionar Tudo"

#: ../../setup_application/shortcuts_settings.rst:282
#, fuzzy
#| msgid "Ctrl+0"
msgid ":kbd:`Ctrl+A`"
msgstr "Ctrl+0"

#: ../../setup_application/shortcuts_settings.rst:283
msgid "Select None"
msgstr "Deseleccionar Tudo"

#: ../../setup_application/shortcuts_settings.rst:283
#, fuzzy
#| msgid "Ctrl++"
msgid ":kbd:`Ctrl+Shift+A`"
msgstr "Ctrl++"

#, fuzzy
#~| msgid "General Shortcuts:"
#~ msgid "Views Shortcuts:"
#~ msgstr "Atalhos Gerais:"

#, fuzzy
#~| msgid "Default Shortcuts"
#~ msgid "Navigation Shortcuts:"
#~ msgstr "Atalhos por Omissão"

#~ msgid "General Shortcuts:"
#~ msgstr "Atalhos Gerais:"

#~ msgid "Close"
#~ msgstr "Fechar"

#~ msgid "Quit"
#~ msgstr "Sair"

#, fuzzy
#~| msgid "Search View"
#~ msgid "Search"
#~ msgstr "Área de Pesquisa"

#, fuzzy
#~| msgid "General Shortcuts:"
#~ msgid "Configure Shortcuts:"
#~ msgstr "Atalhos Gerais:"

#~ msgid "Alt+0"
#~ msgstr "Alt+0"

#~ msgid "Alt+1"
#~ msgstr "Alt+1"

#~ msgid "Alt+2"
#~ msgstr "Alt+2"

#~ msgid "Alt+3"
#~ msgstr "Alt+3"

#~ msgid "none"
#~ msgstr "nenhuma"

#~ msgid "blue"
#~ msgstr "azul"

#~ msgid "gray"
#~ msgstr "cinzento"

# Spanish translations for docs_digikam_org_main_window___dates_view.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Automatically generated, 2022.
# Eloy Cuadra <ecuadra@eloihr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: docs_digikam_org_main_window___dates_view\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-29 01:48+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../main_window/dates_view.rst:1
msgid "digiKam Main Window Dates View"
msgstr "Visor de fechas de la ventana principal de digiKam"

#: ../../main_window/dates_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, dates, calendar, months, weeks, years"
msgstr ""
"digiKam, documentación, manual del usuario, gestión de fotos, gestión "
"fotográfica, código abierto, libre, gratis, aprender, fácil, fechas, "
"calendario, meses, semanas, años"

#: ../../main_window/dates_view.rst:14
msgid "Dates View"
msgstr "Vista de fechas"

#: ../../main_window/dates_view.rst:20
msgid "The digiKam Date View from Left sidebar"
msgstr "La vista de fechas de la barra lateral izquierda de digiKam"

#: ../../main_window/dates_view.rst:22
msgid ""
"The Dates View organizes your photographs based on their dates. digiKam uses "
"either the Exif date or, if no Exif date is available, the last modification "
"time of the file."
msgstr ""
"El visor de fechas organiza las fotos según su fecha. digiKam usa la fecha "
"EXIF o, si no está disponible, la fecha de última modificación del archivo."

#: ../../main_window/dates_view.rst:24
msgid ""
"When you select a month from the list, all images from that month are "
"displayed in the icon-View. You can select days or weeks in the date sheet "
"at the bottom of the Left Sidebar to show only the images from the selected "
"dates."
msgstr ""
"Al seleccionar un mes en la lista, se mostrarán en la vista de iconos todas "
"las imágenes correspondientes a dicho mes. Puede seleccionar días o semanas "
"en la hoja de fechas que hay en la parte inferior de la barra lateral "
"izquierda para mostrar solo las imágenes de las fechas seleccionadas."

# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-02-23 12:31+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../batch_queue/custom_script.rst:1
msgid "digiKam Custom Script from Batch Queue Manager"
msgstr "Aangepast script uit Takenwachtrijbeheerder van digiKam"

#: ../../batch_queue/custom_script.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, custom, script"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, bulk, aangepast, script"

#: ../../batch_queue/custom_script.rst:14
msgid "Custom Script"
msgstr "Aangepast script"

#: ../../batch_queue/custom_script.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../batch_queue/custom_script.rst:19
msgid "Overview"
msgstr "Overzicht"

#: ../../batch_queue/custom_script.rst:21
msgid ""
"The batch Queue Manager allows to customize a **Workflow** with a specific "
"plugin dedicated to run a script and process your images with external tools "
"as `ImageMagick <https://imagemagick.org/>`_ or `ExifTool <https://en."
"wikipedia.org/wiki/ExifTool>`_ for example."
msgstr ""
"De Takenwachtrijbeheerder biedt u het aanpassen van een **Werkmethode** met "
"een specifieke plug-in gericht op het uitvoeren van een script en uw "
"afbeeldingen te bewerken met externe hulpmiddelen zoals, bijvoorbeeld "
"`ImageMagick <https://imagemagick.org/>`_ of `ExifTool <https://en.wikipedia."
"org/wiki/ExifTool>`_."

#: ../../batch_queue/custom_script.rst:23
msgid ""
"The Tool is named **Custom Script**, available in **Base Tools** list, and "
"**Custom Tools** category. The goal is to pass to a script source code "
"written by the user in the plugin, a series of environment variables handled "
"in the code and re-routed for a custom usage with delegate command line "
"programs installed on your computer."
msgstr ""
"Het hulpmiddel is genaamd **Aangepast script**, beschikbaar in de lijst "
"**Basishulpmiddelen**, en categorie **Aangepaste hulpmiddelen**. Het doel is "
"om een script met broncode geschreven door de gebruiker in de plug-in, een "
"serie omgevingsvariabelen behandeld in the code en gestuurd voor een "
"aangepast gebruik met gedelegeerde opdrachtregelprogramma's geïnstalleerd op "
"uw computer."

#: ../../batch_queue/custom_script.rst:29
msgid "The Batch Queue Manager Including a Custom Script in a Workflow"
msgstr ""
"De Takenwachtrijbeheerder die een aangepast script invoegt in een werkmethode"

#: ../../batch_queue/custom_script.rst:31
msgid "The tool provides these options:"
msgstr "Het hulpmiddel levert drie opties:"

#: ../../batch_queue/custom_script.rst:33
msgid ""
"**Output Image Type**: this value allows to setup the expected type of image "
"format to use at the output of your script. The default is **Same as "
"input**, but you can set **JPEG**, **PNG**, or **TIFF**. Take a care that "
"JPEG is a lossy compression format, only support 8-bit color depth, and does "
"not supports transparency."
msgstr ""
"**Type uitvoerafbeelding**: deze waarde biedt het instellen van het "
"verwachte type afbeeldingsformaat te gebruiken bij de uitvoer van uw script. "
"De standaard is **Zelfde als invoer**, maar u kunt **JPEG**, **PNG** of "
"**TIFF** instellen. Let er op dat JPEG een compressieformaat is met verlies, "
"alleen 8-bits kleurdiepte ondersteunt en geen transparantie ondersteunt."

#: ../../batch_queue/custom_script.rst:35
msgid ""
"**Shell Script**: this text edit field allows to enter the source code of "
"your shell script. Under **Linux** and **macOS**, `Bash script <https://en."
"wikipedia.org/wiki/Bash_(Unix_shell)>`_ is supported. Under **Windows** "
"`Batch script <https://en.wikipedia.org/wiki/Batch_file>`_ is supported."
msgstr ""
"**Shell-script**: dit bewerkbare tekstveld biedt het invoeren van de "
"broncode van uw shell-script. Onder **Linux** en **macOS** wordt `Bash "
"script <https://en.wikipedia.org/wiki/Bash_(Unix_shell)>`_ ondersteund. "
"Onder **Windows** wordt `Batch script <https://en.wikipedia.org/wiki/"
"Batch_file>`_ ondersteund."

#: ../../batch_queue/custom_script.rst:37
msgid ""
"The keywords that you can use in your script code are listed below. The tool "
"will replace all occurrences of keywords in script at run time before shell "
"execution. Take a care that keywords are case sensitive."
msgstr ""
"De sleutelwoorden die u in uw script kunt gebruiken staan hier onder. Het "
"hulpmiddel zal alle voorkomens in het script bij uitvoeren vervangen voordat "
"de shell wordt uitgevoerd. Let er op dat sleutelwoorden kleine en "
"hoofdletters onderscheiden."

#: ../../batch_queue/custom_script.rst:39
msgid ""
"**$INPUT** for workflow input filename (with special characters escaped)."
msgstr ""
"**$INPUT** voor bestandsnaam van invoer van werkmethode (met speciale tekens "
"escaped)."

#: ../../batch_queue/custom_script.rst:41
msgid ""
"**$OUTPUT** for workflow output filename (with special characters escaped)."
msgstr ""
"**$OUTPUT** voor bestandsnaam van uitvoer van werkmethode (met speciale "
"tekens escaped)."

#: ../../batch_queue/custom_script.rst:45
msgid ""
"A new file is always expected on **$OUTPUT**. With a script programs that do "
"not create a new file (e.g. changing metadata with ExifTool) you must first "
"copy **$INPUT** to **$OUTPUT** with a command appropriate to the operating "
"system and then make the changes to **$OUTPUT**."
msgstr ""
"Een nieuw bestand wordt altijd verwacht op **$OUTPUT**. Met een script dat "
"geen nieuw bestand maakt (bijv. wijzigen van metagegevens met hulpmiddel "
"Exif) moet u eerst **$INPUT** naar **$OUTPUT** kopiëren met een opdracht "
"geschikt voor het besturingssysteem en daarna de wijzigingen doen op **"
"$OUTPUT**."

#: ../../batch_queue/custom_script.rst:47
msgid ""
"The environment variables that you can use in your script code are listed "
"below:"
msgstr ""
"De omgevingsvariabelen die u kunt gebruiken in uw script worden onderstaand "
"getoond:"

#: ../../batch_queue/custom_script.rst:49
msgid "**TITLE**: to handle digiKam **Title** item properties from database."
msgstr ""
"**TITLE**: om eigenschappen van item **Titel** uit de database digiKam te "
"behandelen."

#: ../../batch_queue/custom_script.rst:51
msgid ""
"**COMMENTS**: to handle digiKam **Caption** item properties from database."
msgstr ""
"**COMMENTS**: om eigenschappen van item **Opschrift** uit de database "
"digiKam te behandelen."

#: ../../batch_queue/custom_script.rst:53
msgid ""
"**COLORLABEL**: to handle digiKam **Color Label** item properties from "
"database."
msgstr ""
"**COLORLABEL**: om eigenschappen van item **Kleurlabel** uit de database "
"digiKam te behandelen."

#: ../../batch_queue/custom_script.rst:55
msgid ""
"**PICKLABEL**: to handle digiKam **Pick Label** item properties from "
"database."
msgstr ""
"**PICKLABEL**: om eigenschappen van item **Keuzelabel** uit de database "
"digiKam te behandelen."

#: ../../batch_queue/custom_script.rst:57
msgid "**RATING**: to handle digiKam **Rating** item properties from database."
msgstr ""
"**RATING**: om eigenschappen van item **Waardering** uit de database digiKam "
"te behandelen."

#: ../../batch_queue/custom_script.rst:59
msgid "**TAGSPATH**: to handle digiKam **Tags** item properties from database."
msgstr ""
"**TAGSPATH**: om eigenschappen van item **Tags** uit de database digiKam te "
"behandelen."

#: ../../batch_queue/custom_script.rst:63
msgid ""
"Under Linux and macOS, environment variables can be accessed in script with "
"**$** as prefix of variable names (for example **$INPUT**). The interpreter "
"used to run the script is **/bin/bash**."
msgstr ""
"Onder Linux en macOS, kunnen omgevingsvariabelen in een script gebruikt "
"worden met **$** als prefix van namen van variabelen (bijvoorbeeld **"
"$INPUT**). De interpreter die het script uitvoert is **/bin/bash**."

#: ../../batch_queue/custom_script.rst:65
msgid ""
"Under Windows, environment variables can be accessed in script with **%** as "
"prefix and suffix of variable names (for example **%INPUT%**). The "
"interpreter used to run the script is **cmd.exe**."
msgstr ""
"Onder Windows kunnen omgevingsvariabelen in een script gebruikt worden met **"
"%** als voor- en achtervoegsel van namen van variabelen (bijvoorbeeld **"
"%INPUT%**). De interpreter die het script uitvoert is **cmd.exe**"

#: ../../batch_queue/custom_script.rst:69
msgid "Return Value"
msgstr "Teruggegeven waarde"

#: ../../batch_queue/custom_script.rst:71
msgid ""
"By convention, a **Bash script** under Linux and macOS, 0 is returned on "
"success or an integer in the range 1-255 for something else. Use **exit < "
"error_code >** to pass the return value on the workflow."
msgstr ""
"De conventie van een **Bash-script** onder Linux en macOS is, dat 0 "
"teruggegeven wordt bij succes of een geheel getal in de reeks van 1-255 voor "
"iets anders. gebruik **exit < error_code >** om de teruggegeven waarde aan "
"de werkmethode door te geven."

#: ../../batch_queue/custom_script.rst:73
msgid ""
"Under Windows, a **Batch script** returns 0 on success and another value for "
"something else, but the value is a signed integer, so a negative value is "
"possible. Use **EXIT /B < error_code >** to pass the return value on the "
"workflow."
msgstr ""
"Onder Windows geeft een **Batch script** een 0 terug bij success en een "
"andere waarde voor iets anders, maar de waarde is een geheel getal met "
"teken, dus een negatieve waarde is mogelijk. gebruik **EXIT /B < error_code "
">** om de teruggegeven waarde aan de werkmethode door te geven."

#: ../../batch_queue/custom_script.rst:75
msgid ""
"The Batch Queue Manager handles the value returned by your script. If zero "
"is returned, the workflow continue as expected, else the workflow is broken "
"and Batch Queue Manager stop the process."
msgstr ""
"De Takenwachtrijbeheerder behandelt de waarde teruggeven door uw script. Als "
"nul wordt teruggegeven zal de werkmethode doorgaan zoals verwacht, anders is "
"het een gebroken werkmethode en de Takenwachtrijbeheerder stopt het proces."

#: ../../batch_queue/custom_script.rst:78
msgid "Examples"
msgstr "Voorbeelden"

#: ../../batch_queue/custom_script.rst:81
msgid "Proof of Concept"
msgstr "Proof of Concept"

#: ../../batch_queue/custom_script.rst:83
msgid ""
"This First example that you can see below, ...do nothing special. It will "
"print on the console the input/output file names and item properties passed "
"from batch queue manager to the script and copy input file to output file "
"(this stage is required else Batch Queue Manager returns an error as the "
"target file does not exist). The script returns the value from the file copy "
"command, this one is parsed by the Batch Queue Manager to check the workflow "
"stream."
msgstr ""
"Dit eerste voorbeeld dat u onderstaand ziet, ...doet niets speciaals. Het "
"zal print op de console de invoer/uitvoer bestandsnamen en eigenschappen van "
"het item doorgegeven uit de takenwachtrijbeheerder naar het script en "
"kopieert invoerbestand naar uitvoerbestand (dit stadium is vereist anders "
"geeft de takenwachtrijbeheerder een fout terug omdat het doelbestand niet "
"bestaat). Het script geeft de waarde terug van het kopiëren van het bestand, "
"deze wordt bekeken door de takenwachtrijbeheerder om de stroom in de "
"werkmethode te controleren."

#: ../../batch_queue/custom_script.rst:100
msgid ""
"If you have started digiKam from a terminal and enabled the debug traces on :"
"ref:`Setup/Miscs/System dialog page <system_settings>`, you will see "
"something like this:"
msgstr ""
"Als u digiKam vanaf een terminal hebt gestart en de debugtraces hebt "
"aangeschakeld :ref:`Dialoogpagina Opzet/Div/Systeem <system_settings>`, dan "
"zult iets zien zoals dit:"

#: ../../batch_queue/custom_script.rst:114
msgid "The digiKam information taken from the database are:"
msgstr "De informatie van digiKam genomen uit de database is:"

#: ../../batch_queue/custom_script.rst:116
msgid "Item processed is **/mnt/data/Images/SALAGOU/DSC08833.JPG**."
msgstr "Verwerkt item is **/mnt/data/Images/SALAGOU/DSC08833.JPG**."

#: ../../batch_queue/custom_script.rst:117
msgid ""
"Target filename is **/mnt/data/Images/SALAGOU/BatchTool-EpEjEz-9e1c7a12."
"digikamtempfile.JPG** (a temporary file generated by Batch Queue Manager)."
msgstr ""
"Doelbestandsnaam is **/mnt/data/Images/SALAGOU/BatchTool-EpEjEz-9e1c7a12."
"digikamtempfile.JPG** (een tijdelijk bestand gegenereerd door "
"takenwachtrijbeheerder)."

#: ../../batch_queue/custom_script.rst:118
msgid "Item Title is **Salagou Trip**."
msgstr "Titel van item is **Salagou Trip**."

#: ../../batch_queue/custom_script.rst:119
msgid "Item Comment is null."
msgstr "Item Commentaar is null."

#: ../../batch_queue/custom_script.rst:120
msgid "Item Color Label is **5** (Green)."
msgstr "Item Kleurlabel is **5** (Groen)."

#: ../../batch_queue/custom_script.rst:121
msgid "Item Pick Label is **3** (Accepted)."
msgstr "Item Keuzelabel is **3** (Geaccepteerd)."

#: ../../batch_queue/custom_script.rst:122
msgid "Item Rating is **3 stars**."
msgstr "Item Waardering is **3 sterren**."

#: ../../batch_queue/custom_script.rst:123
msgid ""
"Item Tags are **Places**, **Places/France**, **Places/France/Salagou Lake**."
msgstr ""
"Item Tags zijn **Plaatsen**, **Plaatsen/Frankrijk**, **Plaatsen/Frankrijk/"
"Salagou Lake**."

#: ../../batch_queue/custom_script.rst:126
msgid "Add a Watermark with ImageMagick"
msgstr "Een watermerk toevoegen met ImageMagick"

#: ../../batch_queue/custom_script.rst:128
msgid ""
"The second example below is more complex and uses **ImageMagick** command "
"like tool to add a multiline text superimposed over pictures to create a "
"visible watermark on the center of images."
msgstr ""
"Het tweede onderstaande voorbeeld is complexer en gebruikt het commando "
"**ImageMagick** als hulpmiddel om een multi-regel tekst over plaatjes te "
"zetten om een zichtbaar watermerk in het centrum van de afbeeldingen te "
"zetten."

#: ../../batch_queue/custom_script.rst:146
msgid ""
"In this example, there is no explicit **exit** call to return a value to the "
"Workflow. Bash uses the last called method as the returned value from the "
"script, here the ImageMagick command line tool **convert**."
msgstr ""
"In dit voorbeeld is er geen expliciete **exit** aanroep om een waarde aan de "
"werkmethode terug te geven. Bash gebruikt de laatst aangeroepen methode als "
"de terugkeerwaarde uit het script, hier de opdracht van ImageMagick "
"**convert**."

#: ../../batch_queue/custom_script.rst:148
msgid "This give a result like below."
msgstr "Dit geeft een resultaat zoals onderstaand."

#: ../../batch_queue/custom_script.rst:154
msgid "The result of the Script Using ImageMagick to Apply a Watermark"
msgstr ""
"Het resultaat van het script met gebruik van ImageMagick om een watermerk "
"toe te passen"

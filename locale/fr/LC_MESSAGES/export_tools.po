msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2022-12-29 18:31+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../export_tools.rst:1
msgid "Export Items From Your Collections To External Media"
msgstr ""

#: ../../export_tools.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"help, learn, export, flickr, google, mediawiki, inaturalist, twitter, "
"pinterest, dropbox, box, onedrive, computer, remote, local"
msgstr ""

#: ../../export_tools.rst:15
msgid "Export Tools"
msgstr ""

#: ../../export_tools.rst:19
msgid "This section explain how to use the digiKam export tools."
msgstr ""

#: ../../export_tools.rst:21
msgid "Contents:"
msgstr ""
